# Allow access to hidden files
c.ContentsManager.allow_hidden = True

# If there is no Python kernelspec registered and the IPython
# kernel is available, ensure it is added to the spec list
c.KernelSpecManager.ensure_native_kernel = False
